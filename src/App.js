import React, { useState } from 'react';
import './App.css';
const axios = require('axios').default;
function App() {
  const [url, setUrl] = useState();
  const [shorturl, setShorturl] = useState();

  return (
    <div className="App">
      <input type="text" placeholder="Enter your url:" onChange={(e) => setUrl(e.target.value)} />
      {url && <button onClick={async () => {
        try {
          const response = await axios({
            method: 'post',
            headers: {
              'Content-Type': 'application/json'
            },
            url: 'https://shorturl-api.herokuapp.com/api',
            data: {
              url: url
            }
          })
          setShorturl(response.data)
        } catch (error) {
          console.error(error)
        }
      }}>Submit!</button>}
      {
        shorturl && <div>
          <h1>Here's your link: </h1>
          <h3>{shorturl}</h3>
          <button onClick={() => navigator.clipboard.writeText(shorturl)}>Copy!</button>
          <button onClick={() => window.open(shorturl, '_blank')}>Open</button>
        </div>

      }
    </div >
  );
}

export default App;
